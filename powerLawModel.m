function spikeRate = powerLawModel(pModel,Vm)
%spikeRate = powerLawModel(pModel,Vm)
%
%One-liner implementing the threshold and power law nonlinearity model 
%that translates from a linear response (e.g. membrane potential) to 
%spike rates.  Here it is:
%
%spikeRate = pModel.k*(max(Vm-pModel.Vrest,0).^pModel.p)

%10/7/08 Written by G.M. Boynton at the University of Washington
spikeRate = pModel.k*(max(Vm-pModel.Vrest,0).^pModel.p);