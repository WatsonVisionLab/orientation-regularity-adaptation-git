function [C,S] = normalizationModelMod(pWorld,pModel,stim,RF,AdapterCurve,AddNoise,poolingRule,number)
%
%Implements Heeger's (1991) contrast normalization model.
%
%Inputs:
%   pWorld             structure of 'world coordinates'
%
%   pModel             structure of model parameters:
%                      'Half-squaring' parameters:
%     pModel.k         scale factor
%     pModel.Vrest     threshold
%     pModel.p         exponent
%                      Normalization parameters:
%     pModel.sig       semisaturation constant
%     pModel.a         scale factor
%
%   stim               2D matrix containing image slices over time
%   RF                 2D matrix containing cell array of RFs
%
%   poolingRule        options for pooling across the array of neurons
%         'all'        provide responses to all RFs (default)
%         'mean'       calculate average of responses
%         'quick'      use 'quick' pooling with quickExp as a power
%
%  Mod                 Vector containing modifications to the
%                      semisaturation constant for each cell or orientaiton
%
%Outputs:
%  C                   complex cell responses
%  S                   simple cell responses
%

%unwrap matric of RFs (e.g. sf x orientation) into one long vector
sz = size(RF);
RF = reshape(RF,prod(sz(1:end-1)),4);
nCells = size(RF,1);

%Match AdapterCurve to unwrapped matrix
if length(size(AdapterCurve)) == 2
for zz = 1:sz(1)
    for tt = 1:4
        UnCurve(zz,:,tt) = AdapterCurve;
    end
end
    Mod = reshape(UnCurve,prod(sz(1:end-1)),4);
else
    Mod = reshape(AdapterCurve,prod(sz(1:end-1)),4);
end

%calculate the linear response and the half-squared response for each RF
clear A E S C
for i=1:nCells
    for j=1:4  %phases
        L = linearResponse(pWorld,stim,RF{i,j});
        A(i,j,:) = powerLawModel(pModel,L+AddNoise);
    end
    E(i,:) = mean(A(i,:,:),2);  %'Energy' output is mean of A's
end

%Average E's (energy) across spatial frequencies and orientations (all cells)
D = squeeze(sum(E,1));  %note: this means that the response is dependent
                        %upon the size of the normalisation pool
                        %(the number of cells present)

%Calculate contrast normalized response (both Single and Complex Cells)
for i=1:nCells  %loop through all cells
    for j=1:4  %loop through quadrature phases
        %simple cell: divide A by energy + semisaturation constant
        S{i}(j,:) = squeeze(pModel.a*A(i,j,:))'./((pModel.sig + Mod(i,j))^2 + D);
    end
    %complex cell: divide E by energy + semisaturation constant
    C(i,:) = pModel.a*E(i,:)./((pModel.sig+Mod(i,j))^2 + D);
end

%reshape simple and complex responses to original dimensions
S = reshape(S,sz(1:end-1));
C = reshape(C,[sz(1:end-1),size(stim,3)]);

NewS = zeros(size(S,1),size(S,2),4);
%Convert S from cell array to SFxOrixPhase Matrix
for ssf = 1:size(S,1)
    for sori = 1:size(S,2)
        for sphase = 1:4
            NewS(ssf,sori,sphase) = S{ssf,sori}(sphase);
        end
    end
end
S = NewS;
%pooling rule

if ~exist('poolingRule','var')
    poolingRule = 'all';
end

switch poolingRule
    case 'mean'
        C = mean(C(:));
    case 'quick'
        C = (sum(C(:).^number))^(1/number);
    case 'index'
        C = C(number(1),number(2));
end

