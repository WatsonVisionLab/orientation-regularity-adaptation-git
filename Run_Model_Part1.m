%Models adaptation to orientation regularity using heeger's (1991)
%normalisation model as the core.
%This implementation is based of G.M. Boynton's work at the University of
%Washington and then modified to fit this experiment by A.Ahmed

clear
clc

%% Basic Info

stim_levels = {'Zero','PionOnetwoeight','PionSixtyfour','PionThirtytwo','PionSixteen','PionEight','PionFour'};
Subject  = {'AA','TW','IM'};
rootpath = 'C:\Example\Model\';
fname = {'Baseline',...
    'Zero'
    'PionThirtyTwo',...
    'PionEight'};
Fieldname = {'one','two','three'};
Runs = 3;

%% Parameter Sweep
%Old Parameter code, just a placeholder at the moment
%All parameters are held constant in this stage
ParamRun = {'a'};
SweepParams = [1];    

%% Model Initilisation

%set up 'world' parameters
%This creates a meshgrid to fit the image in visual angles
pWorld.n = [20,20];         %[ny, nx] in pixels
pWorld.visAngle = 0.6;      %horizontal extent of visual angle
pWorld.background = 0;      %background color: [-1,1]
pWorld = initWorld(pWorld); %get the x and y meshgrid matrice

%Addative Noise parameters, unused in the current implementation
pModel.NoiseSD = 0;
pModel.NoiseMean = 0;

%Half wave rectification and nonlinearity parameters
pModel.k = 1;           %scale factor
pModel.Vrest = 0;       %threshold
pModel.p = 2;           %exponent

%Normalization parameters
pModel.sig = 0.5;        %semisaturation constant at the simple cell level
pModel.a = 1;            %scale factor

%Adaptation parameters
AdaptRule = 1;          %1 = adapters summate; 2 = adapters average
DecisionRule = 1;       %1 = Treat as cell using Naka-Rushton 2 = Compute ratio
AdaptSat = 0.5;         %SemiSaturation for the cell response on the Naka-Rushton using DecisionRule = 1

%% Create a bank of filters

clear pRF RF

sfList = [2.1,2.6,3.1];        %list of preferred spatial frequencies
ctList = [1,1,1];              %list of scale factors for these sf filters
angList = -90:6:89;            %list of preferred orientations
phaseList = [0,90,180,270];    %Quadrature phases

%spatial parameters for Gabor receptive field
pRF.center = [0,0];         %[0,0] is center of image
pRF.sf = NaN;               %'carrier' spatial frequency (c/deg) (to be varied)
pRF.sig = NaN;              %Gaussian standard deviation (to be varied with sf)
pRF.ang = NaN;              %orientation (deg) (to be varied)
pRF.phase = NaN;            %0 = sin phase
pRF.contrast = NaN;         %scale factor (to be varied with sf)
pRF.nt = 1;                 %number of temporal frames

%Generate a cell array containing the receptive fields by calling 'makeGaborRF'
%Columns (1st dimension) will be orientation
%Rows (2nd dimension0 will be spatial frequency
%Third dimension are the four phases (Quadrature phase)
for i=1:length(sfList)
    pRF.sf = sfList(i);         %spatial frequency
    pRF.sig = .33/pRF.sf;       %width is inversely proportional to sf
    pRF.contrast = ctList(i);   %contrast
    for j=1:length(angList)
        pRF.ang = angList(j);   %orientation
        for k=1:4  %quadrature phases
            pRF.phase = phaseList(k);
            RF{i,j,k} = makeGaborRF(pWorld,pRF);
        end
    end
end

%Create Blank adaptation curve
BlankAdaptCurve = zeros([length(angList),1]);

%% Stimulus properties

StimParams = struct(...
        'SF',0.5,...            %Spatial frequency of patches (arbritary units) 1 = 1 cycle/2pi pix
        'GausCon',[5,10],...    %Inner and outer radius of patch sine mask (pix)
        'imagesize',164,...     %Field size (pix)
        'gaborsize',20,...      %Patch size (pix)
        'NumGabor',24,...       %Number of patches
        'Inner',82,...          %Radius of field
        'Blur',40);             %num of Pix edge blur
StimOri = 0;
StimSig = 1;


%% The actual code

%Create structure for data output
POOL = struct;

%Load data files
for ParSw = 1:length(SweepParams)
    AdaptScale = [SweepParams(ParSw),SweepParams(ParSw),SweepParams(ParSw)];
for Subs = 1:length(Subject)
for zz = 1:length(fname)
for rr = 1:Runs
load([rootpath,'Data\RevCor\',Subject{Subs},'_UniformAdaptRevCor_',fname{zz},'_Zero_',num2str(rr),'.mat']);

%Go through each trial
%j indexes arrays within the adapter sequence
j=1;
for i = 1:140
    %load matrix for the current Test fields
    CurrentTest = TestInfo(i,1,1);
    CurrentFixed = TestInfo(i,2,1);
    
    %Load Test
    load ([rootpath,'GaborMatrix\',stim_levels{ResponseArray(i,2)},'.mat'],'Originals','GaborInfo');
    %Clone GaborInfo, this file contains the metainformation for the
    %stimuli (orientation,phase, spatial locations etc.)
    CurrentTestInfo = GaborInfo(:,:,CurrentTest);
    CurrentTestIM = Originals(:,:,CurrentTest);
    %Create cutout, this is an image containing only 1's where a patch was
    %located and 0's everywhere else
    CurrentTestIM(CurrentTestIM==1.000000000000000e-10) = 0;
    CurrentTestIM(CurrentTestIM~=0) = 1;
    
    %Load Test
    load ([rootpath,'GaborMatrix\',stim_levels{4},'.mat'],'Originals','GaborInfo');
    %Clone GaborInfo
    CurrentFixedInfo = GaborInfo(:,:,CurrentFixed);
    CurrentFixedIM = Originals(:,:,CurrentFixed);
    %Create cutout
    CurrentFixedIM(CurrentFixedIM==1.000000000000000e-10) = 0;
    CurrentFixedIM(CurrentFixedIM~=0) = 1;
    
    %-----------------------------------------------------------
    
    if strcmp(fname{zz},'Baseline')
    else
    %Go through adapters
    Overlap = [0,0];
    clear OverlapAdaptS
    clear SeethroughAdaptS
    clear NoPosAdaptS
    clear SecondOrd
    END = 0;
    while END == 0
  
        %Find current Adapter array
        CurrentAdapt(1) = AdaptInfo(j,1,1);
        CurrentAdapt(2) = AdaptInfo(j,2,1);
        
%---------------------------------------------------        

        %Load the Adapter of Interest
        load ([rootpath,'GaborMatrix\',fname{zz},'.mat'],'Originals','GaborInfo');
        CurrentAdaptIM = Originals(:,:,CurrentAdapt(1));
        %Get rid of artifact
        CurrentAdaptIM(CurrentAdaptIM==1.000000000000000e-10) = 0;
        
        %Modelling
        %Create see-through test image, this is the adapter as seen through
        %the task stimuli
        if TestInfo(i,1,2) == 1
            Seethrough = CurrentFixedIM.*CurrentAdaptIM;
            PosClone = CurrentFixedInfo;
        else
            Seethrough = CurrentTestIM.*CurrentAdaptIM;
            PosClone = CurrentTestInfo;
        end
        %-----------(Seethrough)
        %Run adapters as seen through task stimuli
        for gab = 1:StimParams.NumGabor
            CurrentPatch = Seethrough(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1);
            [C,S] = normalizationModelMod(pWorld,pModel,CurrentPatch,RF,BlankAdaptCurve,0);
            SeethroughAdaptS(gab,1,j,:,:,:) = S;
        end
        %-----------(NoPos)
        %Run adapters without consulting task, this is blind to spaital
        %position
        for gab = 1:StimParams.NumGabor
            CurrentPatch = GetGabor (GaborInfo(gab,3,CurrentAdapt(1)), StimParams.SF, StimParams.GausCon, GaborInfo(gab,4,CurrentAdapt(1)), StimParams.gaborsize, 0);
            [C,S] = normalizationModelMod(pWorld,pModel,CurrentPatch,RF,BlankAdaptCurve,0);
            NoPosAdaptS(gab,1,j,:,:,:) = S;
            NoPosC(gab,:,:) = C;
        end
        %-----------(SecondOrd)
        %Second order adaptation can be computed by summing NoPosAdaptS
        TargetDriveArray = sum(sum(NoPosC(:,:,16)));
        AdaptResp = TargetDriveArray/((AdaptSat)^2 + sum(NoPosC(:)));
        SecondOrd(1,j) = AdaptResp;
        %-----------(Overlap)
        %Create Cutout of current adapter array
        CurrentAdaptIM(CurrentAdaptIM~=0) = 1;
        
        %Multiply Adapter and Task cutout using full array
        if TestInfo(i,1,2) == 1
            OvHolder = sum(sum(CurrentFixedIM.*CurrentAdaptIM,1));
        else
            OvHolder = sum(sum(CurrentTestIM.*CurrentAdaptIM,1));
        end
        Overlap(1) = Overlap(1) + OvHolder;
        
        %Repeat for individual patch overlap using Position data (PosClone)
        for gab = 1:StimParams.NumGabor
            OverlapAdaptCell = sum(sum(CurrentTestIM(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1).*CurrentAdaptIM(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1),1));
            %Clone to fit into model
            OverlapClone = ones(30,1)*OverlapAdaptCell;
            OverlapAdaptS(gab,1,j,:) = OverlapClone;
        end
         
%---------------------------------------------------        

       
        %Load the Fixed Adapter
        load ([rootpath,'GaborMatrix\',stim_levels{4},'.mat'],'Originals','GaborInfo');
        CurrentAdaptIM = Originals(:,:,CurrentAdapt(2));
        %Get rid of artifact
        CurrentAdaptIM(CurrentAdaptIM==1.000000000000000e-10) = 0;
        
        %Modelling
        %Create as see-through task image
        if TestInfo(i,1,2) == 1
            Seethrough = CurrentTestIM.*CurrentAdaptIM;
            PosClone = CurrentTestInfo;
        else
            Seethrough = CurrentFixedIM.*CurrentAdaptIM;
            PosClone = CurrentFixedInfo;
        end
        
        %-----------(Seethrough)
        %Run adapters as seen through task stimuli
        for gab = 1:StimParams.NumGabor
            CurrentPatch = Seethrough(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1);
            [C,S] = normalizationModelMod(pWorld,pModel,CurrentPatch,RF,BlankAdaptCurve,0);
            SeethroughAdaptS(gab,2,j,:,:,:) = S;
        end
        %-----------(NoPos)
        %Run adapters without consulting task, this is blind to spaital
        %position
        for gab = 1:StimParams.NumGabor
            CurrentPatch = GetGabor (GaborInfo(gab,3,CurrentAdapt(2)), StimParams.SF, StimParams.GausCon, GaborInfo(gab,4,CurrentAdapt(2)), StimParams.gaborsize, 0);
            [C,S] = normalizationModelMod(pWorld,pModel,CurrentPatch,RF,BlankAdaptCurve,0);
            NoPosAdaptS(gab,2,j,:,:,:) = S;
            NoPosC(gab,:,:) = C;
        end
        %-----------(SecondOrd)
        %Second order adaptation can be computed by summing NoPosAdaptS
        TargetDriveArray = sum(sum(NoPosC(:,:,16)));
        AdaptResp = TargetDriveArray/((AdaptSat)^2 + sum(NoPosC(:)));
        SecondOrd(2,j) = AdaptResp;
        %-----------(Overlap)
        %Create Cutout of current adapter array
        CurrentAdaptIM(CurrentAdaptIM~=0) = 1;
        %Multiply Adapter and Task cutout
        if TestInfo(i,1,2) == 1
            OvHolder = sum(sum(CurrentFixedIM.*CurrentAdaptIM,1));
        else
            OvHolder = sum(sum(CurrentTestIM.*CurrentAdaptIM,1));
        end
        Overlap(2) = Overlap(2) + OvHolder;
        
        %Repeat for individual patch overlap using Position data (PosClone)
        for gab = 1:StimParams.NumGabor
            OverlapAdaptCell = sum(sum(CurrentTestIM(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1).*CurrentAdaptIM(PosClone(gab,1):PosClone(gab,1)+StimParams.gaborsize-1,PosClone(gab,2):PosClone(gab,2)+StimParams.gaborsize-1),1));
            %Clone to fit into model
            OverlapClone = ones(30,1)*OverlapAdaptCell;
            OverlapAdaptS(gab,2,j,:) = OverlapClone;
        end
        
        %Add to loop/exit
        j = j+1;
        if AdaptInfo(j,1,3) ~= i
            END = 1;
        elseif j == length(AdaptInfo)
            END = 1;
        end
    end
    end
    %----------------------------------------------
    
    %Work out model values
    
    if strcmp(fname{zz},'Baseline')
        %Skip if analysing the Baseline condition
    else
    %Fixed task - See through condition
    %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        if AdaptRule == 1
            GaborAdaptMat = sum(SeethroughAdaptS(:,1,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(SeethroughAdaptS(:,1,:,:,:,:),3);
        end
    else
        if AdaptRule == 1
            GaborAdaptMat = sum(SeethroughAdaptS(:,2,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(SeethroughAdaptS(:,2,:,:,:,:),3);
        end
    end
    
    %Clone Values for save file
    SeeThroughMatClone(i,1,:,:,:,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:,:,:,:,:)).*AdaptScale(1),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SeeThroughResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SeeThroughResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Test(controlled) task - See through condition
    %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        if AdaptRule == 1
            GaborAdaptMat = sum(SeethroughAdaptS(:,2,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(SeethroughAdaptS(:,2,:,:,:,:),3);
        end
    else
        if AdaptRule == 1
            GaborAdaptMat = sum(SeethroughAdaptS(:,1,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(SeethroughAdaptS(:,1,:,:,:,:),3);
        end
    end
    
    %Clone Values for save file
    SeeThroughMatClone(i,2,:,:,:,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:,:,:,:,:)).*AdaptScale(1),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SeeThroughResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SeeThroughResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    %--------------------------------------------------
    
    %Fixed task - No Pos condition
    %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        if AdaptRule == 1
            GaborAdaptMat = sum(NoPosAdaptS(:,1,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(NoPosAdaptS(:,1,:,:,:,:),3);
        end
    else
        if AdaptRule == 1
            GaborAdaptMat = sum(NoPosAdaptS(:,2,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(NoPosAdaptS(:,2,:,:,:,:),3);
        end
    end
    
    %Average activity across all cells at all locations
    GaborAdaptMat = squeeze(mean(mean(mean(GaborAdaptMat,1),4),6));
    
    %Clone Values for save file
    NoPosMatClone(i,1,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,GaborAdaptMat.*AdaptScale(2),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoPosResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoPosResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Test(controlled) task - No Pos condition
    %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        if AdaptRule == 1
            GaborAdaptMat = sum(NoPosAdaptS(:,2,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(NoPosAdaptS(:,2,:,:,:,:),3);
        end
    else
        if AdaptRule == 1
            GaborAdaptMat = sum(NoPosAdaptS(:,1,:,:,:,:),3);
        elseif AdaptRule == 2
            GaborAdaptMat = mean(NoPosAdaptS(:,1,:,:,:,:),3);
        end
    end
    
    %Average activity across all cells at all locations
    GaborAdaptMat = squeeze(mean(mean(mean(GaborAdaptMat,1),4),6));

    %Clone Values for save file
    NoPosMatClone(i,2,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,GaborAdaptMat.*AdaptScale(2),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoPosResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoPosResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    %--------------------------------------
    %Second Order Condition
    %Fixed
    if TestInfo(i,1,2) == 1
            GaborAdaptMat = sum(SecondOrd(1,:),2);
    else
            GaborAdaptMat = sum(SecondOrd(2,:),2);
    end
    SecondOrdMat(i,1) = GaborAdaptMat;
    %Test
    if TestInfo(i,1,2) == 1
            GaborAdaptMat = sum(SecondOrd(2,:),2);
    else
            GaborAdaptMat = sum(SecondOrd(1,:),2);
    end
    SecondOrdMat(i,2) = GaborAdaptMat;
    
    end
    
    %--------------------------------------
    %Compute NoAdapt Conditions
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoAdaptResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoAdaptResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoAdaptResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoAdaptResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    %--------------------------------------
    if strcmp(fname{zz},'Baseline')
    else
    %Compute Decision
    if SeeThroughResp(i,1)>SeeThroughResp(i,2)
        SeeThroughResp(i,3) = 1;
    else
        SeeThroughResp(i,3) = 0;
    end
    SeeThroughResp(i,4) = ResponseArray(i,5);
    if SeeThroughResp(i,3) == SeeThroughResp(i,4)
        SeeThroughResp(i,5) = 1;
    else
        SeeThroughResp(i,5) = 0;
    end
    
    if NoPosResp(i,1)>NoPosResp(i,2)
        NoPosResp(i,3) = 1;
    else
        NoPosResp(i,3) = 0;
    end
    NoPosResp(i,4) = ResponseArray(i,5);
    if NoPosResp(i,3) == NoPosResp(i,4)
        NoPosResp(i,5) = 1;
    else
        NoPosResp(i,5) = 0;
    end
    end
    if NoAdaptResp(i,1)>NoAdaptResp(i,2)
        NoAdaptResp(i,3) = 1;
    else
        NoAdaptResp(i,3) = 0;
    end
    NoAdaptResp(i,4) = ResponseArray(i,5);
    if NoAdaptResp(i,3) == NoAdaptResp(i,4)
        NoAdaptResp(i,5) = 1;
    else
        NoAdaptResp(i,5) = 0;
    end
    
    %--------------------------------------
    if strcmp(fname{zz},'Baseline')
    else
   %Translate full array overlap for legacy
   %as it's task-centric as opposed to adapter-centric
    if ResponseArray(i,1)
        OverlapMatClone(i,1) = Overlap(1);
        OverlapMatClone(i,2) = Overlap(2);
    else
        OverlapMatClone(i,1) = Overlap(2);
        OverlapMatClone(i,2) = Overlap(1);
    end
        
        
   %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        GaborAdaptMat = squeeze(sum(OverlapAdaptS(:,2,:,:),3));
    else
        GaborAdaptMat = squeeze(sum(OverlapAdaptS(:,1,:,:),3));
    end
    
    %Clone Values for save file
    OverlapMatCloneIndivid(i,1,:,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:)).*AdaptScale(3),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        OverlapResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        OverlapResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Find the fixed field and apply adapt pooling rule
    if TestInfo(i,1,2) == 1
        GaborAdaptMat = squeeze(sum(OverlapAdaptS(:,1,:,:),3));
    else
        GaborAdaptMat = squeeze(sum(OverlapAdaptS(:,2,:,:),3));
    end
    %Clone Values for save file
    OverlapMatCloneIndivid(i,2,:,:) = squeeze(GaborAdaptMat);
    
    %Run the model for each task patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:)).*AdaptScale(3),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        OverlapResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        OverlapResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    
    %Compute Decision
    if OverlapResp(i,1)>OverlapResp(i,2)
        OverlapResp(i,3) = 1;
    else
        OverlapResp(i,3) = 0;
    end
    OverlapResp(i,4) = ResponseArray(i,5);
    if OverlapResp(i,3) == OverlapResp(i,4)
        OverlapResp(i,5) = 1;
    else
        OverlapResp(i,5) = 0;
    end
    end
    
    
end
    if strcmp(fname{zz},'Baseline')
    else
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SeeThroughResp = SeeThroughResp;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SeeThroughMat = SeeThroughMatClone;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoPosResp = NoPosResp;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoPosRespMat = NoPosMatClone;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapResp = OverlapResp;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMat =OverlapMatClone;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMatIndivid =OverlapMatCloneIndivid;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SecondOrdMat = SecondOrdMat;
    end
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).ResponseArray = ResponseArray;
    POOL.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoAdaptResp = NoAdaptResp;
end
end
end
ParSw
save('ModelOutput_Part1','POOL','-v7.3');
end

save('ModelOutput_Part1','POOL','-v7.3');