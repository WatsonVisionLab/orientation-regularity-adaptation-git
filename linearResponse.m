function resp = linearResponse(pWorld,stim,RF);
%
%resp = linearResponse(pWorld,stim,RF);
%
%Calculates the linear response of the stimulus sequence 'stim' and the
%receptive field 'RF'.  
%
%Parameters:
%
%   stim                2D matrix of size pWorld.n containing stimulus
%                       images
%
%   RF                  2D matrix of size [pWorld.n(1),pWorld.n(2)]
%                       containing linear receptive field images 
%
%   resp                Output vector

%10/7/08 Written by G.M. Boynton at the University of Washington
%20/1/15 Modified by A. Ahmed Western Sydney University

%space and time pixel steps (used for scaling for pixels size)
dx = pWorld.x(1,2) - pWorld.x(1,1);
dy = pWorld.y(2,1) - pWorld.y(1,1);

%reshape stim and RFs to unwrap space into a single dimension
tmpStim = double(reshape(stim,pWorld.n(1)*pWorld.n(2),1))';
tmpRF = reshape(RF,pWorld.n(1)*pWorld.n(2),1)';

resp = tmpStim*tmpRF';  %dot product
resp = resp*dx*dy;      %scaling by pixels size
