function [ParamVal,fval] = Run_Model_Part2_fminsearch(Cond)
%Model optimisaiton
if nargin < 1
    Cond = 'SeeThrough';
    %Can be one of the following 'SeeThrough', 'NoPos', 'Overlap',
    %'SecondOrd'
end

%create a handle for the main modelling function
Handle = @(x) 1/OptModel(x,Cond);

Options = optimset('Display','iter','TolFun',1/5040,'MaxFunEvals',100);

%minimise the model using the scaling factor
[ParamVal,fval] = fminsearch(Handle,1,Options);
end

function Output = OptModel(AdaptScale,Cond)

if nargin < 1
    AdaptScale = 1;
end

%% Basic Info

stim_levels = {'Zero','PionOnetwoeight','PionSixtyfour','PionThirtytwo','PionSixteen','PionEight','PionFour'};
Subject  = {'AA','TW','IM'};
rootpath = 'C:\Example\Model\';
fname = {'Baseline',...
    'Zero'
    'PionThirtyTwo',...
    'PionEight'};
Fieldname = {'one','two','three'};

ParamRun = {'a'};
Runs = 3;
%% Parameter Sweep

    

%% Model Initilisation

%set up 'world' parameters
pWorld.n = [20,20,1];       %[ny, nx, nt] in pixels
pWorld.visAngle = 0.6;      %horizontal extent of visual angle
pWorld.dur = 0;             %total stimulus duration (seconds)
pWorld.background = 0;      %background color: [-1,1]
pWorld = initWorld(pWorld); %get the x and y meshrid matrice

%Addative Noise parameters
pModel.NoiseSD = 0;
pModel.NoiseMean = 0;

%Half wave rectification and nonlinearity parameters
pModel.k = 1;           %scale factor
pModel.Vrest = 0;       %threshold
pModel.p = 2;           %exponent

%Normalization parameters
pModel.sig = 0.5;        %semisaturation constant
pModel.a = 1;           %scale factor

%Adaptation parameters
AdaptRule = 1;          %1 = adapters summate; 2 = adapters average
%AdaptScale = [SweepParams(ParSw),SweepParams(ParSw),SweepParams(ParSw),SweepParams(ParSw)];
%Scaling variable for applying adaptation. AdaptScale(1) = SeeThrough, (2)= NoPos (3) = Overlap (4) = Second Order
DecisionRule = 1;       %1 = Treat as cell using Naka-Rushton 2 = Compute ratio
AdaptSat = 0.5;         %SemiSaturation for the cell response on the Naka-Rushton using DecisionRule = 1

%% Create a bank of filters

clear pRF RF

sfList = [2.1,2.6,3.1];        %list of preferred spatial frequencies
ctList = [1,1,1];           %list of scale factors for these sf filters
angList = -90:6:89;            %list of preferred orientations
phaseList = [0,90,180,270]; %Quadrature phases

%spatial parameters for Gabor receptive field
pRF.center = [0,0];         %[0,0] is center of image
pRF.sf = NaN;               %'carrier' spatial frequency (c/deg) (to be varied)
pRF.sig = NaN;              %Gaussian standard deviation (to be varied with sf)
pRF.ang = NaN;              %orientation (deg) (to be varied)
pRF.phase = NaN;            %0 = sin phase
pRF.contrast = NaN;         %scale factor (to be varied with sf)
pRF.nt = 1;                 %number of temporal frames

%Generate a cell array containing the receptive fields by calling 'makeGaborRF'
%Columns (1st dimension) will be orientation
%Rows (2nd dimension0 will be spatial frequency
%Third dimension are the four phases (Quadrature phase)
for i=1:length(sfList)
    pRF.sf = sfList(i);         %spatial frequency
    pRF.sig = .33/pRF.sf;       %width is inversely proportional to sf
    pRF.contrast = ctList(i);   %contrast
    for j=1:length(angList)
        pRF.ang = angList(j);   %orientation
        for k=1:4  %quadrature phases
            pRF.phase = phaseList(k);
            RF{i,j,k} = makeGaborRF(pWorld,pRF);
        end
    end
end

%Create Blank adaptation curve
BlankAdaptCurve = zeros([length(angList),1]);

%% Stimulus properties

StimParams = struct(...
        'SF',0.5,...            %Spatial frequency of patches (arbritary) 1 = 1 cycle/2pi pix
        'GausCon',[5,10],...    %Inner and outer radius of patch sine mask (pix)
        'imagesize',164,...     %Field size (pix)
        'gaborsize',20,...      %Patch size (pix)
        'NumGabor',24,...       %Number of patches
        'Inner',82,...          %Radius of field
        'Blur',40);             %num of Pix edge blur
StimOri = 0;
StimSig = 1;

%% Placeholder



%% The actual code

%Load Pre-Analysed adapter data
load([rootpath,'ModelOutput_Part1']);
POOLTWO = struct;

%Load data file

for Subs = 1:length(Subject)
for zz = 1:length(fname)
for rr = 1:Runs
    
%load and prepare data
    load([rootpath,'Data\RevCor\',Subject{Subs},'_UniformAdaptRevCor_',fname{zz},'_Zero_',num2str(rr),'.mat']);
    if strcmp(fname{zz},'Baseline')
    else
    SeeThroughMatClone = POOL.a.(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SeeThroughMat;
    NoPosMatClone = POOL.a.(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoPosRespMat;
    OverlapMatClone = POOL.a.(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMat;
    OverlapMatCloneIndivid = POOL.a.(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMatIndivid;
    SecondOrdMat = POOL.a.(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SecondOrdMat;
    end
%Go through each trial
for i = 1:140
    %load matrix for the current Test fields
    CurrentTest = TestInfo(i,1,1);
    CurrentFixed = TestInfo(i,2,1);
    
    %Load Test
    load ([rootpath,'GaborMatrix\',stim_levels{ResponseArray(i,2)},'.mat'],'Originals','GaborInfo');
    %Clone GaborInfo
    CurrentTestInfo = GaborInfo(:,:,CurrentTest);
    CurrentTestIM = Originals(:,:,CurrentTest);
    %Create cutout
    CurrentTestIM(CurrentTestIM==1.000000000000000e-10) = 0;
    CurrentTestIM(CurrentTestIM~=0) = 1;
    
    %Load Test
    load ([rootpath,'GaborMatrix\',stim_levels{4},'.mat'],'Originals','GaborInfo');
    %Clone GaborInfo
    CurrentFixedInfo = GaborInfo(:,:,CurrentFixed);
    CurrentFixedIM = Originals(:,:,CurrentFixed);
    %Create cutout
    CurrentFixedIM(CurrentFixedIM==1.000000000000000e-10) = 0;
    CurrentFixedIM(CurrentFixedIM~=0) = 1;
    
    %-----------------------------------------------------------
 %Work out model values
    
    if strcmp(fname{zz},'Baseline')
        %Skip if analysing the Baseline condition
    else
    %Fixed test - See through condition
    GaborAdaptMat = SeeThroughMatClone(i,1,:,:,:,:);
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(:,:,gab,:,:,:)).*AdaptScale(1),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SeeThroughResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SeeThroughResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Test(controlled) test - See through condition
    GaborAdaptMat = SeeThroughMatClone(i,2,:,:,:,:);
     
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(:,:,gab,:,:,:)).*AdaptScale(1),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SeeThroughResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SeeThroughResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    %--------------------------------------------------
    
    %Fixed test - No Pos condition
    GaborAdaptMat = squeeze(NoPosMatClone(i,1,:));
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,GaborAdaptMat.*AdaptScale(2),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoPosResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoPosResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Test(controlled) test - No Pos condition
    GaborAdaptMat = squeeze(NoPosMatClone(i,2,:));
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,GaborAdaptMat.*AdaptScale(2),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoPosResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoPosResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
        %--------------------------------------
    %Compute SecondOrderOnly Conditions
    %Initial stages treated simiarly to the NoAdapt Condition
    
     %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    
    %The Final step however is modified using the Second order data
    %Compute Regularity
    
    SecOrderVal = SecondOrdMat(i,1);
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SecondOrderResp(i,1) = TargetDrive/((AdaptSat + SecOrderVal*AdaptScale(4))^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SecondOrderResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    
    %The Final step however is modified using the Second order data
    %Compute Regularity
    
    SecOrderVal = SecondOrdMat(i,2);
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        SecondOrderResp(i,2) = TargetDrive/((AdaptSat + SecOrderVal*AdaptScale(4))^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        SecondOrderResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    end
   
    %--------------------------------------    
    %Compute NoAdapt Conditions
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoAdaptResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoAdaptResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,BlankAdaptCurve,0);
        AllC(gab,:,:) = C;
    end
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        NoAdaptResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        NoAdaptResp(i,2) = TargetDrive/sum(AllC(:));
    end

    %--------------------------------------
    if strcmp(fname{zz},'Baseline')
    else
    %Compute Decision
    %We need to not only work out which array the model selected but also
    %whether the model was right and whether it predicted what the
    %participant did
    if SeeThroughResp(i,1)>SeeThroughResp(i,2)
        SeeThroughResp(i,3) = 1;
    else
        SeeThroughResp(i,3) = 0;
    end
    SeeThroughResp(i,4) = ResponseArray(i,5);
    if SeeThroughResp(i,3) == SeeThroughResp(i,4)
        SeeThroughResp(i,5) = 1;
    else
        SeeThroughResp(i,5) = 0;
    end
    
    if NoPosResp(i,1)>NoPosResp(i,2)
        NoPosResp(i,3) = 1;
    else
        NoPosResp(i,3) = 0;
    end
    NoPosResp(i,4) = ResponseArray(i,5);
    if NoPosResp(i,3) == NoPosResp(i,4)
        NoPosResp(i,5) = 1;
    else
        NoPosResp(i,5) = 0;
    end
    
    if SecondOrderResp(i,1)>SecondOrderResp(i,2)
        SecondOrderResp(i,3) = 1;
    else
        SecondOrderResp(i,3) = 0;
    end
    SecondOrderResp(i,4) = ResponseArray(i,5);
    if SecondOrderResp(i,3) == SecondOrderResp(i,4)
        SecondOrderResp(i,5) = 1;
    else
        SecondOrderResp(i,5) = 0;
    end
    end
    if NoAdaptResp(i,1)>NoAdaptResp(i,2)
        NoAdaptResp(i,3) = 1;
    else
        NoAdaptResp(i,3) = 0;
    end
    NoAdaptResp(i,4) = ResponseArray(i,5);
    if NoAdaptResp(i,3) == NoAdaptResp(i,4)
        NoAdaptResp(i,5) = 1;
    else
        NoAdaptResp(i,5) = 0;
    end
    
    %--------------------------------------
    if strcmp(fname{zz},'Baseline')
    else
   
   GaborAdaptMat = squeeze(OverlapMatCloneIndivid(i,1,:,:));
    
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentFixedInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentFixedInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:)).*AdaptScale(3),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        OverlapResp(i,1) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        OverlapResp(i,1) = TargetDrive/sum(AllC(:));
    end
    
   GaborAdaptMat = squeeze(OverlapMatCloneIndivid(i,2,:,:));
   
    %Run the model for each test patch
    for gab = 1:StimParams.NumGabor
        Patch = GetGabor(CurrentTestInfo(gab,3), StimParams.SF, StimParams.GausCon, CurrentTestInfo(gab,4),StimParams.gaborsize, 0);
        [C,S] = normalizationModelMod(pWorld,pModel,Patch,RF,squeeze(GaborAdaptMat(gab,:)).*AdaptScale(3),0);
        AllC(gab,:,:) = C;
    end
    
    %Compute Regularity
    TargetDrive = sum(sum(AllC(:,:,16)));
    if DecisionRule == 1
        OverlapResp(i,2) = TargetDrive/((AdaptSat)^2 + sum(AllC(:)));
    elseif DecisionRule == 2
        OverlapResp(i,2) = TargetDrive/sum(AllC(:));
    end
    
    
    %Compute Decision
    if OverlapResp(i,1)>OverlapResp(i,2)
        OverlapResp(i,3) = 1;
    else
        OverlapResp(i,3) = 0;
    end
    OverlapResp(i,4) = ResponseArray(i,5);
    if OverlapResp(i,3) == OverlapResp(i,4)
        OverlapResp(i,5) = 1;
    else
        OverlapResp(i,5) = 0;
    end
    end
    
    
end
    if strcmp(fname{zz},'Baseline')
    else
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SeeThroughResp = SeeThroughResp;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SeeThroughMat = SeeThroughMatClone;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoPosResp = NoPosResp;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoPosRespMat = NoPosMatClone;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapResp = OverlapResp;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMat =OverlapMatClone;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).OverlapMatIndivid =OverlapMatCloneIndivid;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SecondOrdMat = SecondOrdMat;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).SecondOrdResp = SecondOrderResp;
    end
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).ResponseArray = ResponseArray;
    POOLTWO.(ParamRun{ParSw}).(Subject{Subs}).(fname{zz}).(Fieldname{rr}).NoAdaptResp = NoAdaptResp;
end
end
end

%% Reduce data to a number for model optimisation

AllResults = zeros ([length(Subject),length(fname),length(Fieldname)]);

for ii = 1:length(Subject)
for rr = 1:length(fname)
for nn = 1:length(Fieldname)
    
eval(['AllResults(ii,rr,nn) = sum(POOLTWO.(ParamRun{1}).(Subject{ii}).(fname{rr}).(Fieldname{nn}).',Cond,'Resp(:,5))']);

end
end
end

Output = sum(AllResults(:));

end