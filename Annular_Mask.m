function mask = Annular_Mask (SzE,Inner,Outer,RamP,isCircle)

if nargin < 1
    SzE = 600;
end
if nargin < 2
    Inner = 100;
end
if nargin < 3
    Outer = 200;
end
if nargin < 4
    RamP = 15;
end
if nargin < 5
    isCircle = 0;
end

[x,y] = meshgrid (-SzE/2:SzE/2-1);
radius = sqrt(x.^2 + y.^2);

if ~(isCircle)

    radius(radius <= Inner) = Inner;
    radius(radius >= (Inner+RamP)) = Inner+RamP;
    %normalise and smear radius
    radius = Inner./radius;
    radius = (radius - min (radius(:)))/(max(radius(:))- min(radius(:)));
    %radius as a function of cos
    M1 = (cos ((radius * pi) )/2) +0.5;

    radius = sqrt(x.^2 + y.^2);
    radius(radius <= Outer) = Outer;
    radius(radius >= (Outer+RamP)) = Outer+RamP;
    %normalise and smear radius
    radius = Outer./radius;
    radius = (radius - min (radius(:)))/(max(radius(:))- min(radius(:)));
    %radius as a function of cos
    M2 = (cos ((radius * pi) +pi)/2) +0.5;

    mask = M1.*M2;
else
    
    radius(radius <= Inner) = Inner;
    radius(radius >= (Inner+RamP)) = Inner+RamP;
    %normalise and smear radius
    radius = Inner./radius;
    radius = (radius - min (radius(:)))/(max(radius(:))- min(radius(:)));
    %radius as a function of cos
    mask = (cos ((radius * pi) +pi)/2)+0.5;


end
