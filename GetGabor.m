function [patch] = GetGabor (ori, SF, Sinn, phase, gaborsize, isControl)

if nargin < 6
    isControl = 0;
end

[x,y] = meshgrid (-gaborsize/2:gaborsize/2-1);

%Create Sin Window

radius = sqrt(x.^2 + y.^2);
radiusClone = radius;
radius(radius <= Sinn(1)) = Sinn(1);
radius(radius >= Sinn(2)) = Sinn(2);
%normalise radius
radius = Sinn(1)./radius;
radius = (radius - min (radius(:)))/(max(radius(:))- min(radius(:)));

if isControl
    grat = sin(radiusClone/4+(rand*2*pi));
else
    %Create a grating
    a = cos(ori)*SF;
    b = sin (ori)*SF;
    grat = sin((a*x+b*y)+phase);
end

%radius as a function of cos
mask = (cos ((radius * pi) +pi)/2)+0.5;
patch = grat.*mask;

end
