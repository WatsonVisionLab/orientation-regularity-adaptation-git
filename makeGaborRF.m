function RF = makeGaborRF(pWorld,pRF)
%makeGaborRF(pWorld,pRF)


if length(pRF.sig) == 1
    pRF.sig(2) = pRF.sig(1);
end


rampx = cos(pRF.ang*pi/180)*(pWorld.x-pRF.center(1)) + sin(pRF.ang*pi/180)*(pWorld.y-pRF.center(2));
rampy = sin(pRF.ang*pi/180)*(pWorld.x-pRF.center(1)) - cos(pRF.ang*pi/180)*(pWorld.y-pRF.center(2));


Gauss = exp(-rampx.^2/pRF.sig(2).^2-rampy.^2/pRF.sig(1)^2);


Grating = pRF.contrast*sin(2*pi*pRF.sf*rampx-pRF.phase*pi/180);

RF = Gauss.*Grating;

