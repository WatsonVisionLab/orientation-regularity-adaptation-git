function [image, NumActual] = GetGaborImage_v2 (ori,SD,Signal,Params,isControl)

if nargin < 5
    isControl = 0;
end
if nargin < 4
    Params = struct(...
        'SF',0.5,...
        'GausCon',[5,10],...GausCon = 10;
        'imagesize',164,...
        'gaborsize',20,...
        'NumGabor',24,...
        'Inner',82,...
        'Blur',20,...
        'isCircle',1);
end
if nargin < 1
    ori = 0;
end
if nargin < 2
    SD = pi/8;%pi/16;
end
if nargin < 3
    Signal = 1;
end

Params.isCircle = 1;
Params.Outer = 280;

try
BreakOut = 0;

if pi*(Params.imagesize/2)^2 < Params.NumGabor*(Params.gaborsize^2)
    disp ('Impossible reduce NumGabor or increase imagesize');
    BreakOut = 1;
elseif 0.6*pi*(Params.imagesize/2)^2 < Params.NumGabor*(Params.gaborsize^2)
    disp ('Probably contains less patches than specified');
end

%ori = ori*2*pi/360;
IMmap = zeros (Params.imagesize);

[x,y] = meshgrid (-Params.imagesize/2:Params.imagesize/2-1);
radius = sqrt(x.^2 + y.^2);
for tt = 1:Params.imagesize
    for kk = 1:Params.imagesize
        if Params.isCircle && radius(tt,kk)>= Params.imagesize/2
            IMmap(tt,kk) = 0.0000000001;
        elseif ~Params.isCircle && radius(tt,kk)<= (Params.Inner+Params.Blur)
            IMmap(tt,kk) = 0.0000000001;
        elseif ~Params.isCircle && radius(tt,kk)>= (Params.Outer)
            IMmap(tt,kk) = 0.0000000001;
        end
    end
end

NumSig = ceil(Signal * Params.NumGabor);
Sigzz = SD .* randn ([NumSig,1]);

for i = 1:Params.NumGabor
    
    LocCheck = 0;
    count = 0;
    while LocCheck == 0
    Loc = [randi(Params.imagesize-Params.gaborsize+1),randi(Params.imagesize-Params.gaborsize+1)];
    count = count+1;
        if ~(any(any(IMmap (Loc(1):Loc(1)+Params.gaborsize-1,Loc(2):Loc(2)+Params.gaborsize-1))))
            LocCheck = 1;
        elseif BreakOut
            break
        elseif i>(0.75*Params.NumGabor) && count >= 300
            BreakOut = 1;
            NumActual = i;
            break
        elseif count>= 1000
            disp ('Contains less than 75% NumGabor');
            BreakOut = 1;
            NumActual = i;
            break
        end
    end
    
    if BreakOut
        break
    end
    
    if i >NumSig
        PatchOri = rand*2*pi;
    else
        PatchOri =ori+Sigzz(i);
    end
        
    ThePatch = GetGabor (...
        PatchOri,...
        Params.SF,...
        Params.GausCon,...
        rand*2*pi,...
        Params.gaborsize,...
        isControl);

    IMmap (Loc(1):Loc(1)+Params.gaborsize-1,Loc(2):Loc(2)+Params.gaborsize-1) = ThePatch;
    
    
end

mask = Annular_Mask (Params.imagesize,Params.Inner,Params.Outer,Params.Blur,Params.isCircle);
image = IMmap .* mask;

if ~BreakOut
NumActual = Params.NumGabor;
end

catch exception
    
    throw(exception);
    
end
end

function [patch] = GetGabor (ori, SF, Sinn, phase, gaborsize, isControl)

if nargin < 6
    isControl = 0;
end

[x,y] = meshgrid (-gaborsize/2:gaborsize/2-1);

% %Create a gussian window    
% 
% Gaussian = exp(-((x.^2)+(y.^2))/(GausCon^2));
% patch = grat.*Gaussian;

%Create Sin Window

radius = sqrt(x.^2 + y.^2);
radiusClone = radius;
radius(radius <= Sinn(1)) = Sinn(1);
radius(radius >= Sinn(2)) = Sinn(2);
%normalise and smear radius
radius = Sinn(1)./radius;
radius = (radius - min (radius(:)))/(max(radius(:))- min(radius(:)));

if isControl
    grat = sin(radiusClone/4+(rand*2*pi));
else
    %Create a grating
    a = cos(ori)*SF;
    b = sin (ori)*SF;
    grat = sin((a*x+b*y)+phase);
end

%radius as a function of cos
mask = (cos ((radius * pi) +pi)/2)+0.5;
patch = grat.*mask;

end
